package com.example.programing;

import java.util.Scanner;

public class SecondMaxNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int maxNumber = 0;
        System.out.println("Please enter the Number of items you want to add in add in List");
        String maxNumberInString = sc.nextLine();

        if (maxNumberInString.isEmpty()) {
            System.out.print("The second largest element is: -1");
            System.exit(0);
            return;
        }
        try {
            maxNumber = Integer.parseInt(maxNumberInString);
        } catch (NumberFormatException ex) {
            System.out.println("Error: This is not valid number. Try again with valid number");
            System.exit(0);
        }

        System.out.println("Please enter the values set one by one");

        long[] arr = new long[maxNumber];

        for (int i = 0; i < maxNumber; i++) {
            String value = sc.nextLine();
            try {
                arr[i] = Long.parseLong(value);
            } catch (NumberFormatException ex) {
                System.out.println("Error: This is not valid number. Try again with valid number");
                System.exit(0);
                return;
            }
        }
        long result = getSecondLargest(arr);

        if (result == Long.MIN_VALUE)
            System.out.print("The second largest element is: -1");
        else
            System.out.print("The second largest element is: "+result);


    }

    /* return the second largest element*/
    private static long getSecondLargest(long inputSet[]) {
        long largestNumber, secondLargeNumber;

        /* There should be at-least two elements */
        if (inputSet.length < 2) {
            System.out.print("The second largest element is: -1");
            System.exit(0);
            return Long.MIN_VALUE;
        }

        largestNumber = secondLargeNumber = Long.MIN_VALUE;
        for (int i = 0; i < inputSet.length; i++) {
            /* If current element is smaller than first then update both first and second */
            if (inputSet[i] > largestNumber) {
                secondLargeNumber = largestNumber;
                largestNumber = inputSet[i];
            }

            /* If arr[i] is in between first and second then update second  */
            else if (inputSet[i] > secondLargeNumber && inputSet[i] != largestNumber)
                secondLargeNumber = inputSet[i];
        }
        return secondLargeNumber;
    }

}
