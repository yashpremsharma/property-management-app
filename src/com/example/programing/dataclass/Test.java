package com.example.programing.dataclass;

public class Test {
    public static void main(String[] args) {
        Item item = new Item(1L, 2L);
        System.out.println("First is: "+item.getVal1());
        System.out.println("Second is: "+item.getVal2());
    }
}
