package com.example.programing.apas;

/*class GFG {
    // function to return distinct prime factors

    static Vector<Integer> PrimeFactors(int n)
    {

        // to store distinct prime factors
        Vector<Integer> v = new Vector<Integer>();
        int x = n;

        // run a loop upto sqrt(n)
        for (int i = 2; i * i <= n; i++) {
            if (x % i == 0) {

                // place this prime factor in vector
                v.add(i);
                while (x % i == 0)
                    x /= i;
            }
        }

        // This condition is to handle the case when n
        // is a prime number greater than 1
        if (x > 1)
            v.add(x);

        return v;
    }

    // function that returns good number
    static int GoodNumber(int n)
    {
        // distinct prime factors
        Vector<Integer> v = new Vector<Integer>(PrimeFactors(n));

        // to store answer
        int ans = 1;

        // product of all distinct prime
        // factors is required answer
        for (int i = 0; i < v.size(); i++)
            ans *= v.get(i);

        return ans;
    }

    // Driver code
    public static void main(String[] args)
    {
        int n = 9;

        // function call
        System.out.println(GoodNumber(n));
    }
}*/

/*
class GFG {

    static int MAX = 1000001;
    static int factor[];

    // function to generate all prime
    // factors of numbers from 1 to 10^6
    static void generatePrimeFactors()
    {
        factor[1] = 1;

        // Initializes all the positions with
        // their value.
        for (int i = 2; i < MAX; i++)
            factor[i] = i;

        // Initializes all multiples of 2 with 2
        for (int i = 4; i < MAX; i += 2)
            factor[i] = 2;

        // A modified version of Sieve of
        // Eratosthenes to store the
        // smallest prime factor that
        // divides every number.
        for (int i = 3; i * i < MAX; i++) {
            // check if it has no prime factor.
            if (factor[i] == i) {
                // Initializes of j starting from i*i
                for (int j = i * i; j < MAX; j += i) {
                    // if it has no prime factor
                    // before, then stores the
                    // smallest prime divisor
                    if (factor[j] == j)
                        factor[j] = i;
                }
            }
        }
    }

    // function to calculate number of factors
    static int calculateNoOFactors(int n)
    {
        if (n == 1)
            return 1;

        int ans = 1;

        // stores the smallest prime number
        // that divides n
        int dup = factor[n];

        // stores the count of number of times
        // a prime number divides n.
        int c = 1;

        // reduces to the next number after prime
        // factorization of n
        int j = n / factor[n];

        // false when prime factorization is done
        while (j != 1) {
            // if the same prime number is dividing n,
            // then we increase the count
            if (factor[j] == dup)
                c += 1;

            */
/* if its a new prime factor that is
               factorizing n, then we again set c=1
               and change dup to the new prime factor,
               and apply the formula explained
               above. *//*

            else {
                dup = factor[j];
                ans = ans * (c + 1);
                c = 1;
            }

            // prime factorizes a number
            j = j / factor[j];
        }

        // for the last prime factor
        ans = ans * (c + 1);

        return ans;
    }

    */
/* Driver program to test above function *//*

    public static void main(String[] args)
    {
        // array to store prime factors
        factor = new int[MAX];
        factor[0] = 0;

        // generate prime factors of number
        // upto 10^6
        generatePrimeFactors();

        int a[] = { 1, 5, 9 };

        int q = a.length;

        for (int i = 0; i < q; i++)
            System.out.print(calculateNoOFactors(a[i]) + " ");
    }
}*/


// Java code to find the lexicographically
// smallest string

class GFC {

    // function to sort the
// array of string
    static void sort(String a[], int n)
    {

        //sort the array
        for(int i = 0;i < n;i++)
        {
            for(int j = i + 1;j < n;j++)
            {

                // comparing which of the
                // two concatenation causes
                // lexiographically smaller
                // string
                if((a[i] + a[j]).compareTo(a[j] + a[i]) > 0)
                {
                    String s = a[i];
                    a[i] = a[j];
                    a[j] = s;
                }
            }
        }
    }

    static String lexsmallest(String a[], int n)
    {

        // Sort strings
        sort(a,n);

        // Concatenating sorted strings
        String answer = "";
        for (int i = 0; i < n; i++)
            answer += a[i];

        return answer;
    }

    // Driver code
    public static void main(String args[])
    {
        String a[] = {"hackerearth", "google", "2"};
        int n = 3;
        System.out.println("lexiographically smallest string = "
                + lexsmallest(a, n));

    }
}
