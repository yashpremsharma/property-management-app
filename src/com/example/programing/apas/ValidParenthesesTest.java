package com.example.programing.apas;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class ValidParenthesesTest {

    private HashMap<Character, Character> mappings;

    private ValidParenthesesTest() {
        mappings = new HashMap<>();
        this.mappings.put(')', '(');
        this.mappings.put('}', '{');
        this.mappings.put(']', '[');
    }

    private boolean isValid(String input) {

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // if character is closing brackets
            if (this.mappings.containsKey(c)) {
                // GET THE top element of the stack, if stack is empty, set dummy value of '#'
                char topElement = stack.empty() ? '#' : stack.pop();
                // if mapping for this bracket doesn't match the stack's top element, return false
                if (topElement != this.mappings.get(c)) {
                    return false;
                }


            } else {
                // if character is opening brackets, push to the stack
                stack.push(c);
            }
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the the parentheses");
        String input = sc.nextLine();
        ValidParenthesesTest validParenthesesTest = new ValidParenthesesTest();
        System.out.println(validParenthesesTest.isValid(input));
    }
}
