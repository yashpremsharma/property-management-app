package com.example.programing.apas;

import java.util.Scanner;

public class RomanToIntegerTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the Roman to convert in Integer");
        String input = sc.nextLine();
        System.out.println(romanToInteger(input));
    }

    private static int romanToInteger(String input) {
        int convertedString = 0;

        for (int i = input.length() - 1; i >= 0; i--) {
            switch (input.charAt(i)) {
                case 'M':
                    convertedString += 1000;
                    break;
                case 'D':
                    convertedString += 500;
                    break;
                case 'C':
                    convertedString += 100 * (convertedString >= 500 ? -1 : 1);
                    break;
                case 'L':
                    convertedString += 50;
                    break;
                case 'X':
                    convertedString += 10 * (convertedString >= 50 ? -1 : 1);
                    break;
                case 'V':
                    convertedString += 5;
                    break;
                case 'I':
                    convertedString += (convertedString >= 5 ? -1 : 1);
                    break;
                default:
                    break;
            }
        }
        return convertedString;
    }
}
