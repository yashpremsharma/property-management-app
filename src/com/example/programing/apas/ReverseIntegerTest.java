package com.example.programing.apas;

import java.util.Scanner;

public class ReverseIntegerTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the Integer to be reversed");
        int input = sc.nextInt();
        System.out.println(reverse(input));

    }

    private static int reverse(int x){
        long out = 0;
        while (x != 0){
            out = out * 10 + x % 10;
            x /=  10;
        }

        if (out > Integer.MAX_VALUE || out < Integer.MIN_VALUE) return 0;

        return (int) out;
    }

}
