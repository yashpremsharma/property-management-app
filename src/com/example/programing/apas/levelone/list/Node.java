package com.example.programing.apas.levelone.list;

class Node {
    int data;
    Node next;

    Node(int d) {
        data = d;
        next = null;
    }
}
