package com.example.programing.apas.levelone.list;

public class MergeLists {

    Node head;

    void addToTheLast(Node node) {
        if (head == null) {
            head = node;
        } else {
            Node temp = head;
            while (temp.next != null)
                temp = temp.next;
            temp.next = node;
        }
    }

    /* Method to print linked list */
    void printList() {
        Node temp = head;
        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        MergeLists llist1 = new MergeLists();
        MergeLists llist2 = new MergeLists();

        // Node head1 = new Node(5);
        llist1.addToTheLast(new Node(1));
        llist1.addToTheLast(new Node(2));
        llist1.addToTheLast(new Node(4));

        // Node head2 = new Node(2);
        llist2.addToTheLast(new Node(1));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(4));


        llist1.head = new Gfg().sortedMerge(llist1.head,
                llist2.head);
        llist1.printList();
    }
}
