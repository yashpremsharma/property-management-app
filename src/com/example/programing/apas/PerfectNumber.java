package com.example.programing.apas;

import java.util.Scanner;

public class PerfectNumber {
    private static Scanner sc;

    public static void main(String[] args) {
        int  Number ;
        sc = new Scanner(System.in);
        System.out.println();

        int totalNumbers = sc.nextInt();
        int[] arrNumbers = new int[totalNumbers];

        for (int k = 0; k < totalNumbers; k ++){
            Number = sc.nextInt();
            arrNumbers[k] = Number;
        }

        for (int j = 0; j < arrNumbers.length; j++){
            int Sum = 0;
            int num = arrNumbers[j];
            for(int i = 1 ; i < num ; i++) {
                if(num % i == 0)  {
                    Sum = Sum + i;
                }
            }
            if (Sum == num) {
                System.out.println("YES");
            }
            else {
                System.out.println("NO");
            }
        }


    }
}
