package com.example.programing.apas;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TwoSumTest {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter number of elements in list");
        int n = sc.nextInt();
        int[] arr = new int[n];

        System.out.println("Please enter items on by one");

        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }

        System.out.println("please enter the sum of two numbers");
        int sum = sc.nextInt();
        twoSum(arr, sum);
    }

    private static void twoSum(int[] num, int target) {

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < num.length; i++) {
            int complement = target - num[i];
            if (map.containsKey(complement)){
                System.out.println("Pair with given sum " + target + " is (" + complement + ", " + num[i]  + ")");
            }
            map.put(num[i], i);
        }
    }
}
