package com.example.programing.apas;

import java.util.Scanner;

public class PalindromeTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the Integer to check is it palindrome or not");
        int input = sc.nextInt();
        if (isPalindrome(input)){
            System.out.println("The given number is palindrome");
        } else {
            System.out.println("The given number is not palindrome");
        }
    }

    private static boolean isPalindrome(int value){

        if (value < 0 || (value % 10 == 0 && value != 0)){
            return false;
        }

        int revertedNumber = 0;
        while (value > revertedNumber){
            revertedNumber = revertedNumber * 10 + value % 10;
            value /= 10;

        }

        return value == revertedNumber || value == revertedNumber / 10;
    }
}
