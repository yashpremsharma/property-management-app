package com.example.programing.untitled;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeTest {
    int minDiff(String arrS[], int arr_size) {
        DateFormat df = new SimpleDateFormat("hh:mm aa");
        DateFormat outputformat = new SimpleDateFormat("HH:mm");
        Date date = null;
        int arr[] = new int[arr_size];

        for (int i = 0; i < arr_size; i++) {
            try{
                date= df.parse(arrS[i]);
                System.out.println(toMins(outputformat.format(date)));
                arr[i] = toMins(outputformat.format(date));
            }catch(ParseException pe){
                pe.printStackTrace();
            }
        }

        int min_diff = arr[1] - arr[0];
        int min_element = arr[0];
        int i;
        for (i = 1; i < arr_size; i++) {
            if (arr[i] - min_element < min_diff || min_diff < 0)
                min_diff = arr[i] - min_element;
            if (arr[i] > min_element)
                min_element = arr[i];

            System.out.println("xhxhxhx "+min_diff);
        }
        return min_diff;
    }

    private static int toMins(String s) {
        String[] hourMin = s.split(":");
        int hour = Integer.parseInt(hourMin[0]);
        int mins = Integer.parseInt(hourMin[1]);
        int hoursInMins = hour * 60;
        return hoursInMins + mins;
    }

    public static void main(String[] args) {
        TimeTest mindif = new TimeTest();
        String arr[] = {"1:10 pm", "4:40 am", "5:00 pm"};
//        String arr[] = {"10:00 am", "11:45 pm", "5:00 am", "12:01 am"};
        int size = arr.length;
        System.out.println("MaximumDifference is " + mindif.minDiff(arr, size));
    }
}
