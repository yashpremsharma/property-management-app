package com.example.programing.untitled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.util.Map;
import java.util.Comparator;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class ABC {


    public static void main(String[] args) {
        ArrayList<String> inputString = new ArrayList<>();
        inputString.add("PHONEPE");
        inputString.add("GEEKSFORGEEKS");
        inputString.add("ARPITJAIN");
        inputString.add("YASHBHARDWAJ");
        inputString.add("YASHPRATAPSINGH");
        countUniqueCharacters(inputString);
    }

    public static void countUniqueCharacters(ArrayList<String> inputArray) {

        HashMap<String, Integer> hashMap = new HashMap<>();
        for (int j = 0; j < inputArray.size(); j++) {
            Integer count = 0;
            String input = inputArray.get(j);
            boolean[] isItThere = new boolean[Character.MAX_VALUE];
            for (int i = 0; i < input.length(); i++) {
                isItThere[input.charAt(i)] = true;
            }

            for (int i = 0; i < isItThere.length; i++) {
                if (isItThere[i] == true) {
                    count++;
                }
            }
            System.out.println("hh yashal count is "+count);
            hashMap.put(input, count);
            System.out.println("hh yashal sorted hashmap is "+sortByComparator(hashMap, false));
        }

    }

    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        // Create a list from elements of hashmap
        List<Map.Entry<String, Integer> > list =
                new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }


    private static HashMap<String, Integer> sortByComparator(HashMap<String, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        HashMap<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
