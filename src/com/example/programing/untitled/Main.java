package com.example.programing.untitled;

import java.util.*;
import java.io.*;

class Main {

    public static int StockPicker(int[] arr) {
        // code goes here  
        int maxDifference  = arr[1] - arr[0];
        int minElement = arr[0];
        int i;
        for(i = 1; i < arr.length; i++){
            if(arr[i]- minElement > maxDifference){
                maxDifference = arr[i]- minElement;
            }
            if(arr[i] < minElement){
                minElement = arr[i];
            }
        }
        if(maxDifference > 0)
            return maxDifference;
        else
            return -1;

    }

    public static void main (String[] args) {
        // keep this function call here     
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] intArr = new int[n];
        System.out.println("Enter Number one by one");
        for(int i = 0; i < n; i++){
            intArr[i] = s.nextInt();
        }
        System.out.print(StockPicker(intArr));
    }

}