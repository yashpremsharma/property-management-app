package com.example.programing.mathshomework;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MathsHomeworkTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int minimumNumber = 0;
        int maxNumber = 0;

        System.out.println("Enter the number of the questions For Home Work :: ");
        int noOfQuestion = sc.nextInt();
        int[] pointsArray = new int[noOfQuestion];

        System.out.println("Enter Question points one by one :: ");
        for (int i = 0; i < noOfQuestion; i++) pointsArray[i] = sc.nextInt();

        System.out.println("Initial points of questions are ::" + Arrays.toString(pointsArray));

        Arrays.sort(pointsArray);

        System.out.println("Sorted points of questions are ::" + Arrays.toString(pointsArray));

        System.out.println("Enter the Threshold :: ");
        int thresholdValue = sc.nextInt();

        ArrayList<Integer> pointsArrayTobeSolved = new ArrayList<>();

        for (int i = 0; i < noOfQuestion; i++) {
            System.out.println("Do you want to add the question.. Please write y or n");
            String yesOrNo = sc.next();
            if (yesOrNo.equals("y")) {
                System.out.println("Please enter the number you want to solve, make sure you have to solved the first question, others are optional:: ");
                int questionsToBeSolved = sc.nextInt();
                pointsArrayTobeSolved.add(questionsToBeSolved);
            } else {
                break;
            }

        }

        System.out.println("questions do you want to solve are ::" + pointsArrayTobeSolved);

        for (int i = 0; i < pointsArrayTobeSolved.size(); i++){
            if (i == 0) {
                minimumNumber = pointsArrayTobeSolved.get(0);
            } else if (i == pointsArrayTobeSolved.size()-1){
                maxNumber =  pointsArrayTobeSolved.get(i);
            }
        }

        int difference = maxNumber - minimumNumber;

        if (difference >= thresholdValue){
            System.out.println(" The difference between max and min is "+difference);
        }
    }
}
