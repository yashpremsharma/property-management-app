package com.example.programing.ocrolusdemo.java;

import java.io.*;
import java.util.ArrayList;

public class InputOutputDemo {

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayListCSVData = new ArrayList<>();
        ArrayList<String> outPut = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "/Users/zolo13/Desktop/Kotlin_workspace/Demo/src/com/example/programing/ocrolusdemo/resources/search.text"));
            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String csvFile = "/Users/zolo13/Desktop/Kotlin_workspace/Demo/src/com/example/programing/ocrolusdemo/resources/phone_dataset.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] country = line.split(cvsSplitBy);
                arrayListCSVData.add(country[0] + ", " + country[1] + ", " + country[2] + ", " + country[3]);

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < arrayList.size(); i++) {
            outPut.add("Matches for: " + arrayList.get(i));
            System.out.println("Matches for: " + arrayList.get(i));
            boolean isFound = false;
            int count = 0;
            for (int j = 0; j < arrayListCSVData.size(); j++) {
                if (arrayListCSVData.get(j).toLowerCase().contains(arrayList.get(i).toLowerCase())) {
                    count++;
                    isFound = true;
                    outPut.add("Result " + count + ": " + arrayList.get(i) + ", " + arrayListCSVData.get(j));
                    System.out.println("Result " + count + ": " + arrayList.get(i) + ", " + arrayListCSVData.get(j));
                }

            }
            if (!isFound) {
                outPut.add("No results found");
                System.out.println("No results found");
            }
        }

        // saving the data in output file
        for (int i = 0; i < outPut.size(); i++){
            try {
                FileWriter writer = new FileWriter("/Users/zolo13/Desktop/Kotlin_workspace/Demo/src/com/example/programing/ocrolusdemo/resources/output.text", true);
                writer.write(outPut.get(i));
                writer.write("\r\n");   // write new line
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
