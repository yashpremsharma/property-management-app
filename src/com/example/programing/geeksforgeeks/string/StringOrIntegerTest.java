package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class StringOrIntegerTest {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        takeInput();
    }

    private static void askChoice() {
        System.out.println("Do you want to test continue: give your choice in Y or N");
        switch (sc.nextLine().toUpperCase()) {

            case "Y":
                takeInput();
                break;
            default:
                System.exit(0);
                break;

        }
    }

    private static void takeInput() {
        System.out.println("Please enter the input");
        String str = sc.nextLine();

        if (isNumber(str))
            System.out.println("The given input is Number");
        else
            System.out.println("The given input is String");

        askChoice();
    }

    private static boolean isNumber(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i)) == false)
                return false;
        }
        return true;
    }

}
