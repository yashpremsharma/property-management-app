package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class StringSameOrNotTest {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        takeInput();
    }

    private static void askChoice() {
        System.out.println("Do you want to test continue: give your choice in Y or N");
        switch (sc.nextLine().toUpperCase()) {

            case "Y":
                takeInput();
                break;
            default:
                System.exit(0);
                break;

        }
    }

    private static void takeInput() {
        System.out.println("Please enter the name");
        String name = sc.nextLine();
        if (allCharactersSame(name)){
            System.out.println(" All Characters are same");
            askChoice();
        } else {
            System.out.println(" All Characters are not same");
            askChoice();
        }
    }

    private static boolean allCharactersSame(String str){

        for (int i = 0; i < str.length(); i++){
            if (str.charAt(i) != str.charAt(0))
                return false;
        }

        return true;
    }
}
