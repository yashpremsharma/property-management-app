package com.example.programing.geeksforgeeks.string;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CopyStringIterative {
    public static void main (String[] args) throws IOException {
        InputStreamReader ir = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(ir);
        System.out.println("Enter First String");
        String input = br.readLine();
        char[] s1 = input.toCharArray();
        System.out.println("Enter Second String");
        String input2 = br.readLine();
        char[] s2 = new char[s1.length];
        input2.toCharArray();
        myCopy(s1, s2);
        System.out.println(String.valueOf(s2));
        br.close();
        ir.close();
    }

    static void myCopy(char s1[], char s2[])
    {
        int i = 0;
        for (i = 0; i < s1.length; i++)
            s2[i] = s1[i];
    }
}
