package com.example.programing.geeksforgeeks.string

data class TestData(
    val name: String,
    val department: String
)