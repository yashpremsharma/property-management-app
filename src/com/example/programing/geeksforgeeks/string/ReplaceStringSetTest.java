package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class ReplaceStringSetTest {

    static char[] alphabets = "abcdefghijklmnopqrstuvwxyz".toCharArray();


    public static void main(String[] args) {
        String convertedString = "qwertyuiopasdfghjklzxcvbnm";
        char[] charSet = convertedString.toCharArray();

        Scanner sc = new Scanner(System.in);
        System.out.println("The string set is qwertyuiopasdfghjklzxcvbnm");
        System.out.println("Enter the string which want to replace");
        String str = sc.nextLine();
//        str = conversion(charSet, str);
        str = conversion(convertedString, str.toCharArray());
        System.out.println(str);
    }

    private static String conversion(char[] charSet, String str){

        int n = str.length();

        // hashing for new character set
        char hashChar[] = new char[26];
        for (int i = 0; i < 26; i++) {

            hashChar[Math.abs(charSet[i] - 'a')] = (char) ('a' + i);
        }
        // conversion of new character set
        String s="";
        for (int i = 0; i < n; i++) {
            s += hashChar[str.charAt(i) - 'a'];
        }
        return s;
    }

    static String conversion(String charSet, char[] str1)
    {
        String s2 = "";
        int temp = 0;
        for (char i : str1) {
            temp = charSet.indexOf(i);
            System.out.println(temp);
            s2 += alphabets[temp];
        }
        return s2;
    }
}
