package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class DuckNumberTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number: ");
        String num1 = sc.nextLine();

        char firstDigit = num1.charAt(0);
        System.out.println("First digit is: "+firstDigit);

        if (checkDuck(num1) > 0 && firstDigit != '0')
            System.out.println("This number is duck number");
        else
            System.out.println("This number is not duck number");
    }

    private static int checkDuck(String num){

        int len = num.length();
        int countZero = 0 ;
        char ch;

        for (int i =0; i < len; i++){
            ch = num.charAt(i);
            if (ch == '0')
                countZero++;
        }

        return countZero;
    }

}
