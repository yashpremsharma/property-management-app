package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class RoundNumberTest {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n = sc.nextInt();

        System.out.println(round(n));
    }

    private static int round(int n) {
        //smaller multiple
        int a = (n/10)*10;
        int b = a + 10;

        return (n-a > b-n)? b : a;
    }

}
