package com.example.programing.geeksforgeeks.string;

import java.util.Scanner;

public class InitialNameStringTest {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        takeInput();
    }

    private static void printInitials(String name) {

        if (name.length() == 0)
            return;

        String[] words = name.split(" ");
        for (String word : words) {
            System.out.print(Character.toUpperCase(word.charAt(0)) + " ");
        }
        System.out.println();
        askChoice();

    }

    private static void askChoice() {
        System.out.println("Do you want to test continue: give your choice in Y or N");
        switch (sc.nextLine().toUpperCase()) {

            case "Y":
                takeInput();
                break;
            default:
                System.exit(0);
                break;

        }
    }

    private static void takeInput() {
        System.out.println("Please enter the name");
        String name = sc.nextLine();
        printInitials(name);
    }

}
