package com.example.programing.geeksforgeeks.arrays

fun main() {

    val arrayName = arrayOf(1, 2, 3, 4, 5)

    for(i in 0 until arrayName.size){
        print(" ${arrayName[i]}")
    }

    println()

    val arrayName2 = arrayOf<Int>(10, 20, 30, 40, 50)
    for(i in 0 until arrayName2.size){
        print(" ${arrayName2[i]}")
    }

    println()

    val arrayName3 = Array(5) { i -> i * 1 }
    for(i in 0 until arrayName3.size){
        println(" ${arrayName3[i]}")
    }
    println(" the value on 2nd position is ${arrayName3.get(1)}")
    arrayName3.set(3, 10)
    for(i in 0 until arrayName3.size){
        println(" ${arrayName3[i]}")
    }
    println(" the value on 4th position is ${arrayName3[3]}")
    arrayName3[4] = 100
    for(i in 0 until arrayName3.size){
        println(" ${arrayName3[i]}")
    }

    println("*********Array Traversing using for loop with indices*************")

    val num = arrayOf<Int>(1, 2, 3, 4, 5)
    num[0] = 10
    num[1] = 6
    for (i in num.indices)
    {
        println(num[i])
    }
    println("*********Array Traversing using for loop with ranges*************")

    for (i in 0..num.size-1){
        println(num[i])
    }

    println("*********Array Traversing using for each  *************")

    num.forEach { index -> println(index)}
}