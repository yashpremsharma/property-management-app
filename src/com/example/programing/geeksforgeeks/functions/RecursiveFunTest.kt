package com.example.programing.geeksforgeeks.functions

fun main() {
    var n = 5
    callMe(n)
    println("Factorial of 5 is: "+Fact(5))
}

fun callMe(a: Int){
    println(a)
    if (a > 0) callMe(a-1)
}

fun Fact(num: Int):Long{
    return if(num==1) num.toLong()        // terminate condition
    else num*Fact(num-1)
}