package com.example.programing.geeksforgeeks.functions

fun main() {

    company("Yash", "Shubham", "Deendayal")

    val result1 = sum1(2,3)
    val result2 = sum2(3,4)
    println("The sum of two numbers is: $result1")
    println("The sum of two numbers is: $result2")

    // directly print the return value of lambda
    // without storing in a variable.
    println(sum1(5,7))

}

val sum1 = { a: Int, b: Int -> a + b }

// without type annotation in lambda expression
val sum2: (Int, Int) -> Int = { a, b -> a + b }

val company =
    { ceo: String, cfo: String, cto: String -> println("ExamWarriors partners names are $ceo, $cfo, $cto") }
