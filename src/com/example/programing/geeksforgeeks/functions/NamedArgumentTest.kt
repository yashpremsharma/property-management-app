package com.example.programing.geeksforgeeks.functions

fun main() {
    val name = "Arpit"
    val standard = "VIII"
    val rollNo = 1
    studentNamed(name = name,roll_no = rollNo)

}

fun studentNamed(name: String="Yash", standard: String="XII" , roll_no: Int=100) {
    println("Name of the student is: $name")
    println("Standard of the student is: $standard")
    println("Roll no of the student is: $roll_no")
}