package com.example.programing.geeksforgeeks.functions

fun main() {
    val name = "Arpit"
    val rollNo = 1
    val grade = 'A'
    student(name,rollNo,grade)
    student("Yash Bhardwaj", 100, 'A')
    println("The multiplication of two numbers is: ${mul(10,5)}")
}

private fun student(name: String , roll_no: Int , grade: Char) {
    println("Name of the student is : $name")
    println("Roll no of the student is: $roll_no")
    println("Grade of the student is: $grade")
}


fun mul(a: Int, b: Int): Int {
    var number = a.times(b)
    return number
}