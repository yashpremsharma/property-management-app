package com.example.programing.isarelationship;

public class Child extends Parent {

    public static void main(String[] args) {
        System.out.println("Print with Parent Reference and Parent Object");
        Parent p = new Parent();
        p.m1();
        p.m2();
//        p.m3();
        System.out.println("Print with Child Reference and Child Object");
        Child c = new Child();
        c.m3();
        c.m1();
        c.m2();

        System.out.println("Print with Parent Reference and Child Object");
        Parent p1 = new Child();
        p1.m1();
        p1.m2();

//        p1.m3();
        System.out.println("Print with Child Reference and parent Object");
//        Child c2 = new Parent();
    }

    public void m3(){
        System.out.println("M3 method invoked");
    }
}
