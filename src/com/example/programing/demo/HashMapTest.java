package com.example.programing.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapTest {
    public static void main(String[] args){
        HashMap<String,Integer> hmap = new HashMap<String,Integer>();
        Scanner in = new Scanner(System.in);

        int N = in.nextInt();

        for(int i=0;i<N;i++){
            System.out.println("Enter "+(i+1)+"th Key");
            String b=in.next();
            System.out.println("Enter "+(i+1)+"th Value");
            Integer a=in.nextInt();
            hmap.put(b,a);
        }
        for(Map.Entry m:hmap.entrySet()) {
            System.out.println(m.getKey()+" "+m.getValue());
        }
    }


}
