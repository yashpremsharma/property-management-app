package com.example.programing.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

public class NumberGroup {

    public static void main(String[] args){


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int N = 0;
        try {
            N = Integer.parseInt(br.readLine().trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[] A = new int[N];
        String[] inp = new String[0];
        try {
            inp = br.readLine().trim().split(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < N; i++) {
            A[i] = Integer.parseInt(inp[i]);
        }
        ArrayList<Integer> arr = solve(N, A);
        for (int i = 0; i < arr.size(); i++) {
            wr.print(arr.get(i));
            if (i == arr.size() - 1) wr.println();
            else wr.print(" ");
        }
        wr.close();
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static  ArrayList<Integer> solve(int N, int[] A) {
        // Your code goes here
        HashMap<Integer, Integer> arrayListMap = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            int count = 0;
            if (arrayListMap.size() == 0) {
                arrayListMap.put(A[i], 0);
            }
            if (arrayListMap.size() > 0) {
                if(arrayListMap.get(A[i]) != null) {
                count = arrayListMap.get(A[i]);
//                if (count == 0) {
                    count++;
                }
                else {
                    count = 1;
                }
            }
            arrayListMap.put(A[i], count);
        }
        arrayListMap = sortByComparator(arrayListMap,false);
        ArrayList<Integer> keys = new ArrayList<>(arrayListMap.keySet());
        return keys;
    }

    private static HashMap<Integer, Integer> sortByComparator(HashMap<Integer, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2)
            {
                int returnType = 0;
                if (order)
                {
                    returnType =  o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    returnType = o2.getValue().compareTo(o1.getValue());
                }
                return returnType;
            }
        });

        // Maintaining insertion order with the help of LinkedList
        HashMap<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }


}
