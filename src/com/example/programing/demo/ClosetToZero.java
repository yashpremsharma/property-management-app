package com.example.programing.demo;

import java.io.*;
import java.util.*;

public class ClosetToZero {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int N = Integer.parseInt(br.readLine().trim());
        int[] A = new int[N];
        String[] arr_A = br.readLine().split(" ");
        for(int i_A = 0; i_A < N; i_A++) {
            A[i_A] = Integer.parseInt(arr_A[i_A]);
        }
        int out_ = Solve(N, A);
        System.out.println("Nearest to Zero is:"+out_);

        wr.close();
        br.close();
    }
    static int Solve(int N, int[] A){
        // Write your code here
        int closestIndex = 0;

        Arrays.sort(A);
        int diff = Integer.MAX_VALUE;
        for (int i = 0; i < A.length; ++i) {
            int abs = Math.abs(A[i]);
            if (abs < diff) {
                closestIndex = i;
                diff = abs;
            } else if (abs == diff && A[i] > 0 && A[closestIndex] < 0) {
                closestIndex =i;
            }
        }
        return A[closestIndex];
    }
}