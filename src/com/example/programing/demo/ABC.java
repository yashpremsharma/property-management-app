package com.example.programing.demo;

public class ABC {

    public static void main(String[] args) {
        Person p1 = new Person("Yash");
        Person p2 = new Person("Arpit");
        swap(p1, p2);
        System.out.println("Name of P1 is "+p1.name);
        System.out.println("Name of P2 is "+p2.name);

        System.out.println("Executing Rename FUnction");
        reName(p1);
        System.out.println("Name of P1 after Rename is "+p1.name);

    }

    public static void swap(Person p1, Person p2){
        Person temp = p1;
        p1 = p2;
        p2 = temp;
    }

    public static void reName(Person p1){
        p1.setName("Pratap");
    }
}



