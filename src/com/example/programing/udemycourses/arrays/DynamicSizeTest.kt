package com.example.programing.udemycourses.arrays

fun main(args: Array<String>) {

    var partyNames = Array<String>(20) { "" }

    println("How many people do you want to invite the party? But make sure this party hall is enough for 20 peoples......")

    var numberOfPeople: Int = Integer.valueOf(readLine())
    var invitedPeopleSize = numberOfPeople-1

    if(numberOfPeople == 0) {
        println("Oh. uh... You don't want to invite any one")
        return
    } else if (numberOfPeople > 0 && numberOfPeople < 21) {
        println("Please input names of your guest")
        for (myVar in 0..invitedPeopleSize) {
            partyNames[myVar] = readLine()!!
        }

        for (myVar in 0..invitedPeopleSize) {
            println(partyNames[myVar] + " is Invited to the party")
        }
    } else {
        println("Oh. uh... Your Hall is not enough for your guests")
        return
    }


}