package com.example.programing.udemycourses.arrays

fun main(args: Array<String>) {

    var partyNames = Array<String>(10){""}

    partyNames[0] = "Yash Bhardwaj"

    println(partyNames[0])

    partyNames[1] = "Shalu Bhardwaj"

    println(partyNames[1])

    println()
    println("All Party names")

    for (myVar in partyNames){
        println(myVar)
    }

    println()
    println("Party Name using indexes")

    partyNames[2] = "Prem Kumar Sharma"
    partyNames[3] = "Shashi Kala Sharma"
    partyNames[4] = "Shubha Bhardwaj"
    partyNames[5] = "Uma Bhardwaj"
    partyNames[6] = "Kavisha Sharma"
    partyNames[7] = "Rohit Kumar"
    partyNames[8] = "Ankush Agarwal"
    partyNames[9] = "Sourabh Nayak"

    for (myVar in 0..9){
        println(partyNames[myVar])
    }

    println()
    println("All Party Names Invited by the user")

    for (myVar in 0..9){
        partyNames[myVar] = readLine()!!
        println(partyNames[myVar]+" is Invited to the party")
    }

}