package com.example.programing.udemycourses.oops

fun main(args: Array<String>) {

    var myCar = Car()
    println("Before setting the Values")
    println("myCar speed ${myCar.speed}")
    println("myCar power ${myCar.power}")
    println("myCar numberOfWheels ${myCar.numberOfWheels}")
    println("myCar name ${myCar.name}")

    println()
    println("After setting the Values")
    myCar.speed = 200
    myCar.power = 1000
    myCar.numberOfWheels = 4
    myCar.name = "Swift"
    println("myCar speed ${myCar.speed}")
    println("myCar power ${myCar.power}")
    println("myCar numberOfWheels ${myCar.numberOfWheels}")
    println("myCar name ${myCar.name}")


    var yourCar = Car()

    println()

    println("Before setting the Values")
    println("yourCar speed ${yourCar.speed}")
    println("yourCar power ${yourCar.power}")
    println("yourCar numberOfWheels ${yourCar.numberOfWheels}")
    println("yourCar name ${yourCar.name}")

    println()
    println("After setting the Values")
    yourCar.speed = 100
    yourCar.power = 800
    yourCar.numberOfWheels = 4
    yourCar.name = "Alto"
    println("myCar speed ${yourCar.speed}")
    println("myCar power ${yourCar.power}")
    println("myCar numberOfWheels ${yourCar.numberOfWheels}")
    println("myCar name ${yourCar.name}")

    println()

    println("Object name ${Car().name}")

}

class Car {

    var speed: Int = 0
    var power: Int = 0
    var numberOfWheels: Int = 0
    var name: String = ""

}