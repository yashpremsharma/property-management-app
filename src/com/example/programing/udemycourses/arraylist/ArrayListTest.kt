package com.example.programing.udemycourses.arraylist

fun main(args: Array<String>) {

    var partyNames = ArrayList<String>()
    partyNames.add("Yash")
    partyNames.add("Prem")
    partyNames.add("Shalu")
    partyNames.add("Shashi")
    partyNames.add("Kavi")
    partyNames.add("Shubha")
    partyNames.add("Uma")

    println("The first person invited to the party "+partyNames.get(0))
    println("The Second person invited to the party "+partyNames.get(1))
    println("The Third person invited to the party "+partyNames.get(2))
    println("The Fourth person invited to the party "+partyNames.get(3))
    println("The Fifth person invited to the party "+partyNames.get(4))
    println("The SIxth person invited to the party "+partyNames.get(5))
    println("The Seventh person invited to the party "+partyNames.get(6))

    println()

    for (myVar in partyNames){
        println(myVar)
    }

    println()
    partyNames.set(2, "Prasoon")
    println(partyNames.get(2))

    println()

    println("Print all names")
    for (myVar in partyNames){
        println(myVar)
    }

    println()

    if (partyNames.contains("Prasoon")){
        println("Wow.. Prasoon is also invited")
    }else{
        println("Sorry.. Prasoon is not invited")
    }

    println()

    println("Party list by index: " )

    for (myVar in 0..partyNames.size-1){
        println(partyNames.get(myVar))
    }

    println()

    partyNames.remove("Prasoon")

    for (myVar in 0..partyNames.size-1){
        println(partyNames.get(myVar))
    }

    println()

    partyNames.removeAt(3)

    for (myVar in 0..partyNames.size-1){
        println(partyNames.get(myVar))
    }

}