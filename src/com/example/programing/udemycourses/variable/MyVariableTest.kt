package com.example.programing.udemycourses.variable

fun main() {

    var myPlaceHolder = "My Value"
    var myNumber = 20

    println(myPlaceHolder)
    println(myNumber)

}