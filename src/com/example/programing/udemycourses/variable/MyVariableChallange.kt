package com.example.programing.udemycourses.variable

fun main() {

    var myFirstFavAnimal = "Cow"
    var mySecondFavAnimal = "Hourse"
    var myThirdFavAnimal = "Rabbit"
    var myFourthFavAnimal = "Dog"

    println("My Favourites animals are:")
    println(myFirstFavAnimal)
    println(mySecondFavAnimal)
    println(myThirdFavAnimal)
    println(myFourthFavAnimal)

}