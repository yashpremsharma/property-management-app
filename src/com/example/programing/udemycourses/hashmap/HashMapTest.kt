package com.example.programing.udemycourses.hashmap

fun main(args: Array<String>) {

    var dictionary = HashMap<String, String>()

    dictionary.put("A", "One")
    dictionary.put("B", "Two")
    dictionary.put("C", "Three")
    dictionary.put("D", "Four")
    dictionary.put("E", "Five")

    println(dictionary["A"])
    println(dictionary["B"])
    println(dictionary["C"])
    println(dictionary.get("D"))
    println(dictionary.get("E"))

    println()
    println("Printing in Keys")
    for (myVar in dictionary.keys){
        println(myVar)
    }

    println("Printing in Values")
    for (myVar in dictionary.values){
        println(myVar)
    }

    println()
    println("Updating hashmap")
    dictionary.put("C", "Yash")
    println(dictionary["C"])

    println()
    println("Deleteing Items from Dictionary")
    dictionary.remove("D")

    println()
    println("Printing in Keys")
    for (myVar in dictionary.keys){
        println(myVar)
    }

    println("Printing in Values")
    for (myVar in dictionary.values){
        println(myVar)
    }



}