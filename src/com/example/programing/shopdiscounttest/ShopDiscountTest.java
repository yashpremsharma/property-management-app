package com.example.programing.shopdiscounttest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ShopDiscountTest {

    public static void main(String[] args) {
        int minimum = 0;
        int total = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of the items to sale :: ");
        int noOfItems = sc.nextInt();
        int itemsArray[] = new int[noOfItems];
        ArrayList<Integer> itemsArrayListUpdated = new ArrayList<>();
        System.out.println("Enter item price one by one :: ");
        for (int i = 0; i < noOfItems; i++) {
            itemsArray[i] = sc.nextInt();
        }
        System.out.println("Initial price of items are ::"+Arrays.toString(itemsArray));

        int length = itemsArray.length;

        for (int i = 0; i < length-1; i++) {
            int value = itemsArray[i];
            if (i < length-1) {
                minimum = itemsArray[i + 1];
            }

            for (int j = i + 1; j < length; j++) {
                if (itemsArray[j] < minimum) {
                    minimum = itemsArray[j];
                }
            }
            if (minimum <= value) {
                value = value - minimum;
            }
            itemsArrayListUpdated.add(value);

        }
        itemsArrayListUpdated.add(itemsArray[itemsArray.length-1]);
        System.out.println("the updated list of items is:: "+itemsArrayListUpdated);

        for (int abc : itemsArrayListUpdated) {
            total = total + abc;
        }

        System.out.println("The total of the items price is "+total);
    }

}
