package com.example.programing.durgascjp.collection.cursors;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public class CursorClassImplementationDemo {

    public static void main(String[] args) {

        Vector vector = new Vector();
        Enumeration enumeration = vector.elements();
        Iterator iterator = vector.iterator();
        ListIterator listIterator = vector.listIterator();
        System.out.println(enumeration.getClass().getName());
        System.out.println(iterator.getClass().getName());
        System.out.println(listIterator.getClass().getName());

    }
}
