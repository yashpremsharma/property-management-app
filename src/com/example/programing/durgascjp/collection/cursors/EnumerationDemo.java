package com.example.programing.durgascjp.collection.cursors;

import java.util.Enumeration;
import java.util.Vector;

public class EnumerationDemo {

    public static void main(String[] args) {
        Vector vector = new Vector();
        for (int i = 0; i <= 10; i++) {
            vector.addElement(i);
        }
        System.out.println(vector); //[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        System.out.println("getting value one by one using for loop and vector size");
        for (int i = 0; i <vector.size() ; i++) {
            if (i%2 == 0){
                System.out.println(i); /*0
                                         2
                                         4
                                         6
                                         8
                                        10*/
            }
        }
        System.out.println("getting value one by one using for Enumeration");
        Enumeration  e = vector.elements();
        while (e.hasMoreElements()){
            Integer integer =  (Integer)e.nextElement();
            if (integer%2 == 0){
                System.out.println(integer); /*0
                                               2
                                               4
                                               6
                                               8
                                             10*/
            }
        }
        System.out.println(vector); //[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
}
