package com.example.programing.durgascjp.collection.cursors;

import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorDemo {

    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add("Balakrishna");
        linkedList.add("Venki");
        linkedList.add("Chiru");
        linkedList.add("Naga");
        System.out.println(linkedList); //[Balakrishna, Venki, Chiru, Naga]
        ListIterator listIterator = linkedList.listIterator();
        while (listIterator.hasNext()) {
            String s = (String) listIterator.next();
            if (s.equals("Venki")) {

                listIterator.remove(); //[Balakrishna, Chiru, Naga]

            } else if (s.equals("Naga")) {

                listIterator.add("Chaitu"); //[Balakrishna, Chiru, Naga, Chaitu]

            } else if (s.equals("Chiru")) {

                listIterator.set("Charan"); //[[Balakrishna, Charan, Naga, Chaitu]

            }
        }
        System.out.println(linkedList);
    }
}
