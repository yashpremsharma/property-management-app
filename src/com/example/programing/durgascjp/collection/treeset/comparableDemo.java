package com.example.programing.durgascjp.collection.treeset;

public class comparableDemo {
    public static void main(String[] args) {
        System.out.println("A".compareTo("Z")); //-25
        System.out.println("k".compareTo("K")); //15
        System.out.println("A".compareTo("A")); //0
//        System.out.println("A".compareTo(null)); //Exception in thread "main" java.lang.NullPointerException
    }
}
