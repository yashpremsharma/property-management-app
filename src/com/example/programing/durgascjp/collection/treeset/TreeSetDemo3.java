package com.example.programing.durgascjp.collection.treeset;


// WAP to insert StringBuffer objects into the TreeSet where sorting order is Alphabetical Order
import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetDemo3 {

    public static void main(String[] args) {
        TreeSet treeSet = new TreeSet(new MyComparator());
        treeSet.add(new StringBuffer("A"));
        treeSet.add(new StringBuffer("Z"));
        treeSet.add(new StringBuffer("K"));
        treeSet.add(new StringBuffer("L"));
        System.out.println(treeSet); //[A, K, L, Z]
    }

    static class MyComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            String name1 = o1.toString();
            String name2 = o2.toString();
//            return -name2.compareTo(name1);
            return name1.compareTo(name2);
        }
    }
}
