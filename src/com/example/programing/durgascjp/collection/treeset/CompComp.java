package com.example.programing.durgascjp.collection.treeset;

import java.util.Comparator;
import java.util.TreeSet;

class Employee implements Comparable {
    String name;
    int eid;

    public Employee(String name, int eid) {
        this.name = name;
        this.eid = eid;
    }
    public String toString(){
        return name  + "---" + eid;
    }

    public int compareTo(Object ob) {
         int eid1 = this.eid;
         Employee e = (Employee)ob;
         int eid2 = e.eid;

         if (eid1 < eid2) {
             return -1;
         } else if (eid1 > eid2) {
             return 1;
         } else {
             return 0;
         }
    }
}

public class CompComp {

    public static void main(String[] args) {
        Employee e1 = new Employee("Nag", 100);
        Employee e2 = new Employee("Balaiah", 200);
        Employee e3 = new Employee("Chiru", 50);
        Employee e4 = new Employee("Venki", 150);
        Employee e5 = new Employee("Nag", 100);

        TreeSet treeSet = new TreeSet();
        treeSet.add(e1);
        treeSet.add(e2);
        treeSet.add(e3);
        treeSet.add(e4);
        treeSet.add(e5);
        System.out.println(treeSet);  // [Chiru---50, Nag---100, Venki---150, Balaiah---200]

        TreeSet treeSet1 = new TreeSet(new MyComparator1());
        treeSet1.add(e1);
        treeSet1.add(e2);
        treeSet1.add(e3);
        treeSet1.add(e4);
        treeSet1.add(e5);
        System.out.println(treeSet1);  //[Balaiah---200, Chiru---50, Nag---100, Venki---150]



    }
}

class MyComparator1 implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Employee e1 = (Employee) o1;
        Employee e2 = (Employee) o2;
        String s1 = e1.name;
        String s2 = e2.name;

        return s1.compareTo(s2);
    }
}
