package com.example.programing.durgascjp.collection.treeset;

// WAP to insert String and StringBuffer objects into TreeSet where sorting order is increasing length order.
// If 2 objects having same length consider their alphabetical order

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetDemo4 {

    public static void main(String[] args) {

        TreeSet treeSet = new TreeSet(new MyComparator());
        treeSet.add("A");
        treeSet.add(new StringBuffer("ABC"));
        treeSet.add(new StringBuffer("AA"));
        treeSet.add("XX");
        treeSet.add("ABCD");
        treeSet.add("A");
        System.out.println(treeSet); //[A, AA, XX, ABC, ABCD]

    }

    static class MyComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            String name1 = o1.toString();
            String name2 = o2.toString();
            int length1 = name1.length();
            int length2 = name2.length();

            if (length1 < length2)
                return -1;
            else if (length1 > length2)
                return 1;
            else
                return name1.compareTo(name2);

        }
    }
}
