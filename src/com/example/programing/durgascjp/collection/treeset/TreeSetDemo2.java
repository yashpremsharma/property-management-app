package com.example.programing.durgascjp.collection.treeset;

// WAP to insert String objects into the TreeSet where all elements should be inserted according to reverse of Alphabetical Order

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetDemo2 {

    public static void main(String[] args) {
        TreeSet treeSet = new TreeSet(new MyComparator());
        treeSet.add("Roja");
        treeSet.add("ShobhaRani");
        treeSet.add("Rajakumari");
        treeSet.add("GangaBhawani");
        treeSet.add("Ramulamma");
        System.out.println(treeSet); //[ShobhaRani, Roja, Ramulamma, Rajakumari, GangaBhawani]
    }

    static class MyComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            String name1 = o1.toString();
            String name2 = o2.toString();
//            return name2.compareTo(name1);
            return -name1.compareTo(name2);
        }
    }
}
