package com.example.programing.durgascjp.collection.treeset;

import java.util.TreeSet;

public class TreeSetDemo1 {

    public static void main(String[] args) {

        TreeSet treeSet = new TreeSet();
        treeSet.add(new StringBuffer("A"));
        treeSet.add(new StringBuffer("Z"));
        treeSet.add(new StringBuffer("B"));
        treeSet.add(new StringBuffer("L"));
        System.out.println(treeSet); //Exception in thread "main" java.lang.ClassCastException: java.lang.StringBuffer cannot be cast to java.lang.Comparable

    }
}
