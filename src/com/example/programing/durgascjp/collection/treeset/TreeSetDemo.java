package com.example.programing.durgascjp.collection.treeset;

import java.util.TreeSet;

public class TreeSetDemo {

    public static void main(String[] args) {

        TreeSet treeSet = new TreeSet();
        treeSet.add("A");
        treeSet.add("a");
        treeSet.add("B");
        treeSet.add("Z");
        treeSet.add("L");
//        treeSet.add(new Integer(10)); //Exception in thread "main" java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
//        treeSet.add(null);  //Exception in thread "main" java.lang.NullPointerException
        System.out.println(treeSet); //[A, B, L, Z, a]

    }
}
