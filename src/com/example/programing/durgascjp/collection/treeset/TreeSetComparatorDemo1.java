package com.example.programing.durgascjp.collection.treeset;


// WAP to insert Integer objects into the TreeSet where sorting order is descending Order

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetComparatorDemo1 {

    public static void main(String[] args) {

        TreeSet treeSet = new TreeSet(new MyComparator());
        treeSet.add(10);
        treeSet.add(0);
        treeSet.add(15);
        treeSet.add(5);
        treeSet.add(20);
        treeSet.add(20);
        System.out.println(treeSet);

    }

    static class MyComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Integer integer1 = (Integer) o1;
            Integer integer2 = (Integer) o2;
//        return integer1.compareTo(integer2);  // [0, 5, 10, 15, 20]
//        return -integer1.compareTo(integer2);  // [20, 15, 10, 5, 0]
//        return integer2.compareTo(integer1);  // [20, 15, 10, 5, 0]
//        return -integer2.compareTo(integer1);  // [0, 5, 10, 15, 20]
//        return +1;  //  [10, 0, 15, 5, 20, 20]
//        return -1;  //  [20, 20, 5, 15, 0, 10]
            return 0;    // [10]
        }
    }

}


