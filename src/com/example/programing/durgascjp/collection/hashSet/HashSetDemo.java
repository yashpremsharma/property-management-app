package com.example.programing.durgascjp.collection.hashSet;

import java.util.HashSet;

public class HashSetDemo {

    public static void main(String[] args) {
        HashSet hashSet = new HashSet();
        System.out.println(hashSet.add("A"));  //true
        System.out.println(hashSet.add("A"));  //false
        hashSet.add("B");
        hashSet.add("C");
        hashSet.add("D");
        hashSet.add("Z");
        hashSet.add(null);
        hashSet.add(10);
        System.out.println(hashSet.add("Z")); //false
        System.out.println(hashSet); //[null, D, B, A, C, Z, 10] // output order can't expect because  insertion order  not preserved
    }
}
