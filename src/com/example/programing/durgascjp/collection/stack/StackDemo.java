package com.example.programing.durgascjp.collection.stack;

import java.util.Stack;

public class StackDemo {

    public static void main(String[] args) {

        Stack stack = new Stack();
        System.out.println(stack.empty());  //true
        stack.push("A");
        stack.push("B");
        stack.push("C");
        System.out.println(stack.capacity()); //10
        System.out.println(stack); //[A, B, C]
        System.out.println(stack.search("A")); //3
        System.out.println(stack.search("Z")); //-1
        System.out.println(stack.empty());  //false
        stack.pop();
        System.out.println(stack); //[A, B]
    }
}
