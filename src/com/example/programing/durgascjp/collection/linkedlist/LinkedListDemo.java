package com.example.programing.durgascjp.collection.linkedlist;

import java.util.LinkedList;

public class LinkedListDemo {

    public static void main(String[] args) {

        LinkedList linkedList = new LinkedList();
        linkedList.add("Yash");
        linkedList.add(30);
        linkedList.add(null);
        linkedList.add("Yash");
        System.out.println(linkedList); // [Yash, 30, null, Yash]
        linkedList.set(0, "Software");
        System.out.println(linkedList);  //[Software, 30, null, Yash]

        linkedList.add(0, "Vinky");
        System.out.println(linkedList); // [Vinky, Software, 30, null, Yash]

        linkedList.removeLast();
        System.out.println(linkedList);  // [Vinky, Software, 30, null]

        linkedList.addFirst("CCC");
        System.out.println(linkedList);  // [CCC, Vinky, Software, 30, null]

    }
}
