package com.example.programing.durgascjp.collection.vector;

import java.util.Vector;

public class VectorDemo {

    public static void main(String[] args) {

        Vector v = new Vector();
//        Vector v = new Vector(24);
//        Vector v = new Vector(10, 5);
        System.out.println(v.capacity());

        for (int i= 1; i <= 10; i++) {
            v.add(i);
        }
        System.out.println(v);
        System.out.println(v.capacity());
        v.addElement("A");
        System.out.println(v.capacity());
        System.out.println(v);

    }
}
