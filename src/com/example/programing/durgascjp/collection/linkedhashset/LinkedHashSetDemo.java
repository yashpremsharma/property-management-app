package com.example.programing.durgascjp.collection.linkedhashset;

import java.util.LinkedHashSet;
import java.util.SortedSet;

public class LinkedHashSetDemo {

    public static void main(String[] args) {

        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add("B");
        linkedHashSet.add("C");
        linkedHashSet.add("D");
        linkedHashSet.add("Z");
        linkedHashSet.add(null);
        linkedHashSet.add(10);
        System.out.println(linkedHashSet.add("Z")); //false
        System.out.println(linkedHashSet); //[B, C, D, Z, null, 10]

    }

}
