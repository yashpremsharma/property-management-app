package com.example.programing.durgascjp.MultiThreading.interrupt;

public class InterruptThreadDemo {

    public static void main(String[] args) {
        MyThread t = new MyThread();
        t.start();
        t.interrupt();
        System.out.println("End of main");
    }

}
