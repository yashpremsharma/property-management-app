package com.example.programing.durgascjp.MultiThreading.interrupt;

public class MyThread1 extends Thread{

    public void run() {

        for (int i = 1; i <= 10000; i++){
            System.out.println("I am lazy Thread "+i);
        }
        System.out.println("I am entering into sleeping Thread ");
        try {
                Thread.sleep(2000);
        } catch (InterruptedException e){
            System.out.println("I am Interrupted");
        }
    }
}
