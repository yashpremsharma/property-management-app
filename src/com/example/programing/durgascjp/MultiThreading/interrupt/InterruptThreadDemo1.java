package com.example.programing.durgascjp.MultiThreading.interrupt;

public class InterruptThreadDemo1 {

    public static void main(String[] args) {
        MyThread1 t = new MyThread1();
        t.start();
        t.interrupt();
        System.out.println("End of main");
    }

}
