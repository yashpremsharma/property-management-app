package com.example.programing.durgascjp.MultiThreading.implementrunnable;

public class RunnableDemoTest {

    public static void main(String[] args) {

        MyRunnable r = new MyRunnable();
        Thread t1 = new Thread();
        Thread t2 = new Thread(r);
        System.out.println("*****t1.start() calling*****");
        t1.start();
        System.out.println("*****t1.run() calling*****");
        t1.run();

        System.out.println("*****t2.start() calling*****");
        t2.start();
        System.out.println("*****t2.run() calling*****");
        t2.run();

//        System.out.println("*****r.start() calling*****");
//        r.start(); can not find symbol
        System.out.println("*****r.run() calling*****");
        r.run();
        for (int i = 0; i < 10; i++){
            System.out.println("Main Thread "+(i+1));
        }
    }

}