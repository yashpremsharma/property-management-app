package com.example.programing.durgascjp.MultiThreading.synchronizedmethod;

public class SynchronizedDemo1 {

    public static void main(String[] args) {
        Display1 d1 = new Display1();
        MyThread1 t1 = new MyThread1(d1);
        MyThread2 t2 = new MyThread2(d1);
        t1.start();
        t2.start();
    }
}
