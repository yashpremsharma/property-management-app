package com.example.programing.durgascjp.MultiThreading.synchronizedblock;

public class MyThread1 extends Thread {

    Display1 d;
    String name;

    public MyThread1(Display1 d, String name){
        this.d = d;
        this.name = name;
    }

    public void run() {
        d.wish(name);
    }
}
