package com.example.programing.durgascjp.MultiThreading.synchronizedblock;

public class SynchronizedDemo1 {

    public static void main(String[] args) {
        Display1 d1 = new Display1();
        Display1 d2 = new Display1();
        MyThread1 t1 = new MyThread1(d1, "Dhoni");
        MyThread1 t2 = new MyThread1(d2, "Yuvraj");
        t1.start();
        t2.start();
    }
}
