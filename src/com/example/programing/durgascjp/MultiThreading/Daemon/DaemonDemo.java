package com.example.programing.durgascjp.MultiThreading.Daemon;

public class DaemonDemo {

    public static void main(String[] args) {

        System.out.println(Thread.currentThread().isDaemon());
//        Thread.currentThread().setDaemon(true);
        MyThread thread = new MyThread();
        System.out.println(thread.isDaemon());
        thread.setDaemon(true);
        System.out.println(thread.isDaemon());
    }


}
class MyThread extends Thread {

}
