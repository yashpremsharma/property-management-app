package com.example.programing.durgascjp.MultiThreading.Daemon;

public class DaemonThreadDemo {

    public static void main(String[] args) {

        MyThread1 thread = new MyThread1();
        thread.setDaemon(true);
        thread.start();
        System.out.println("End of main thread");
    }


}

class MyThread1 extends Thread {

    public void run(){
        for (int i = 0; i  < 10; i++){
            System.out.println("Child  Thread");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
