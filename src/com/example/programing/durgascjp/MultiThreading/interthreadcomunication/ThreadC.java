package com.example.programing.durgascjp.MultiThreading.interthreadcomunication;


public class ThreadC {

    public static void main(String[] args) throws Exception{

        ThreadD b = new ThreadD();
        b.start();
//        Thread.sleep(10000);
        synchronized(b){
            System.out.println("Main Thread tried to call wait method");
//            b.wait(10000);
            b.wait();
            System.out.println("Main thread got notification");
            System.out.println(b.total);
        }

    }
}
