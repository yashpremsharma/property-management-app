package com.example.programing.durgascjp.MultiThreading.interthreadcomunication;

public class ThreadD extends Thread {

    int total = 0;
    public void run() {
        synchronized (this){
            System.out.println("Child thread starts calculation");
            for (int i = 1; i <= 100; i++){
                total = total + i;
            }
            System.out.println("Child thread giving Notifications");
            this.notify();
        }

    }
}
