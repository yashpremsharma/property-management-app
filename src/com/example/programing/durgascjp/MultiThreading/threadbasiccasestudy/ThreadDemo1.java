package com.example.programing.durgascjp.MultiThreading.threadbasiccasestudy;

// Overriding run method
public class ThreadDemo1 {


    public static void main(String[] args) {

        MyThread1 t1 = new MyThread1();
        t1.start();
        t1.run(10);
    }

}
class MyThread1 extends Thread {

    public void run() {
        System.out.println("No arg run method");
    }

    public void run(int i) {
        System.out.println("Int arg run method");
    }
}