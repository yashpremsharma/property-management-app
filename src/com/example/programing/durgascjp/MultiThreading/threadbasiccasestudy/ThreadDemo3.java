package com.example.programing.durgascjp.MultiThreading.threadbasiccasestudy;

//If we are  overriding start method
public class ThreadDemo3  {

    public static void main(String[] args) {
        MyThread3 t = new MyThread3();
        t.start();
        System.out.println("Main Method");
      //  t.start();  Exception in thread "main" java.lang.IllegalThreadStateException
    }
}

class MyThread3 extends Thread {

    public void start(){
        super.start();
        System.out.println("Start Method");
    }

    public void run(){
        System.out.println("Run Method");
    }
}
