package com.example.programing.durgascjp.MultiThreading.namingthread;

public class ThreadNamingDemo1 {

    public static void main(String[] args) {
        MyThreadName1 t = new MyThreadName1();
        t.start();
        System.out.println("Main method is executed by Thread: "+Thread.currentThread().getName());
    }
}

class MyThreadName1 extends Thread {

    public void run(){
        System.out.println("run method is executed by: "+Thread.currentThread().getName());
    }

}
