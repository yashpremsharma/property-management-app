package com.example.programing.durgascjp.MultiThreading.priority;

public class ThreadPriorityDemo {

    public static void main(String[] args) {
        System.out.println("MIN_PRIORITY "+Thread.MIN_PRIORITY);
        System.out.println("MAX_PRIORITY "+Thread.MAX_PRIORITY);
        System.out.println("NORM_PRIORITY "+Thread.NORM_PRIORITY);
        System.out.println("main thread's default priority: "+Thread.currentThread().getPriority());
//        Thread.currentThread().setPriority(15);  Exception in thread "main" java.lang.IllegalArgumentException
        Thread.currentThread().setPriority(7);
        System.out.println("main thread's priority after setting priority: "+Thread.currentThread().getPriority());
        MyThread t = new MyThread();
        t.setPriority(10);
        t.start();
        System.out.println("child thread's priority: "+t.getPriority());

        for (int i = 0; i < 10; i++){
            System.out.println("Main Thread "+(i+1));
        }
    }
}
