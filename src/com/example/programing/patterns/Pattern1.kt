package com.example.programing.patterns

fun main(args: Array<String>) {
    println("hh yashal")

    pattern1()
    pattern2()
    pattern3()
    pattern4()
    pattern5()
    pattern6()
    pattern7()
    pattern()
    patternReverse()
    patternAlphabet()
    patternAlphabetReverse()
}

fun pattern1() {
    println("Printing pattern 1")
    for (item in 1..5) {
        for (itemNew in 1..5) {
            print("*")
        }
        println()
    }
}

fun pattern2() {
    println("*******************Printing pattern 2 ********************")
    for (item in 1..5) {
        for (itemNew in 1..5) {
            print(item)
        }
        println()
    }
}

fun pattern3() {
    println("*******************Printing pattern 3 ********************")
    for (item in 1..5) {
        for (itemNew in 1..5) {
            print(itemNew)
        }
        println()
    }
}

fun pattern4() {
    println("******************* Printing pattern 4 ********************")
    for (item in 'A'..'E') {
        for (itemNew in 1..5) {
            print(item)
        }
        println()
    }
}

fun pattern5() {
    println("******************* Printing pattern 5 ********************")
    for (item in 1..5) {
        for (itemNew in 'A'..'E') {
            print(itemNew)
        }
        println()
    }
}

fun pattern6() {
    println("******************* Printing pattern 6 ********************")
    for (item in 5 downTo 1) {
        for (itemNew in 1..5) {
            print(item)
        }
        println()
    }
}

fun pattern7() {
    println("******************* Printing pattern 7 ********************")
    for (item in 1..5) {
        for (itemNew in 5 downTo 1) {
            print(itemNew)
        }
        println()
    }
}

fun pattern() {
    println("******************* Printing pattern ********************")
    val rangeStart = 1..7 step 2
    for (item in rangeStart) {
        for (itemNew in rangeStart) {
            if (item == itemNew) {
                print(item)
                break
            } else {
                print("  ")
            }
        }
        println()
    }
}

fun patternReverse() {
    println("******************* Printing pattern Reverse ********************")
    val rangeStart = 7 downTo 1 step 2
    for (item in rangeStart) {
        for (itemNew in rangeStart) {
            if (item == itemNew) {
                print(item)
                break
            } else {
                print("  ")
            }
        }
        println()
    }
}

fun patternAlphabet() {
    println("******************* Printing pattern Reverse ********************")
    val rangeStart = 'A'..'I' step 2
    for (item in rangeStart) {
        for (itemNew in rangeStart) {
            if (item == itemNew) {
                print(item)
                break
            } else {
                print("  ")
            }
        }
        println()
    }
}

fun patternAlphabetReverse() {
    println("******************* Printing pattern Reverse ********************")
    val rangeStart = 1..9
    for (item in rangeStart) {
        for (itemJ in 9 downTo item) {
            if (item % 2 == 0) {
                break
            } else {
                print(" ")
            }
        }
        for (itemNew in item downTo 1) {
            if (item % 2 == 0) {
                println()
                break
            } else {
                print(itemNew)
            }
        }
    }
    println()
}
