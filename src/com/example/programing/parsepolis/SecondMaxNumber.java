package com.example.programing.parsepolis;
import java.math.BigInteger;
import java.util.Scanner;

public class SecondMaxNumber {

    private static final BigInteger MIN_VALE = BigInteger.valueOf(0x8000000000000000L);
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int maxNumber = 0;
        System.out.println("Please enter the Number of items you want to add in add in List");
        String maxNumberInString = sc.nextLine();

        if (maxNumberInString.isEmpty()) {
            System.out.print("The second largest element is: -1");
            System.exit(0);
            return;
        }
        try {
            maxNumber = Integer.parseInt(maxNumberInString);
        } catch (NumberFormatException ex) {
            System.out.println("Error: This is not valid number. Try again with valid number");
            System.exit(0);
        }

        System.out.println("Please enter the values set one by one");

        BigInteger[] arr = new BigInteger[maxNumber];

        for (int i = 0; i < maxNumber; i++) {
            String value = sc.nextLine();
            try {
                if (value.length() < 1024) {
                    arr[i] = new BigInteger(value);
                } else {
                    i--;
                    System.out.println("Error: Not a valid number. Try again with valid number");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Error: This is not valid number. Try again with valid number");
                System.exit(0);
                return;
            }
        }
        BigInteger result = getSecondLargest(arr);
        if (result.equals(MIN_VALE))
            System.out.print("The second largest element is: -1");
        else
        System.out.print("The second largest element is: "+result);


    }

    /* return the second largest element*/
    private static BigInteger getSecondLargest(BigInteger[] inputSets) {
        BigInteger largestNumber = MIN_VALE, secondLargeNumber = MIN_VALE;

        /* There should be at-least two elements */
        if (inputSets.length < 2) {
            System.out.print("The second largest element is: -1");
            System.exit(0);
            return MIN_VALE;
        }

        for (BigInteger input: inputSets) {
            /* If current element is smaller than first then update both first and second */
            int compareValue = input.compareTo(largestNumber);
            int compareValueSecond = input.compareTo(secondLargeNumber);
            if (compareValue > 0) {
                secondLargeNumber = largestNumber;
                largestNumber = input;
            } else if (compareValueSecond > 0 && !input.equals(largestNumber)) {
                secondLargeNumber = input;
            }

        }
        return secondLargeNumber;
    }

}
