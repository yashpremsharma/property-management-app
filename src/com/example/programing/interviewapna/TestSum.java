package com.example.programing.interviewapna;

import java.util.HashSet;

public class TestSum {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 5, 7, 3, 0, 4};
        int n = 7;
        printPairs(arr, n);
//        doStuff(arr, n);
    }

    public static void doStuff(int[] arr, int var){
        for (int i =0; i < arr.length; i++){
            for (int j = i+1; j < arr.length; j++){
                if (arr[i]+arr[j] == var){
                    System.out.println("values are "+arr[i]+", "+arr[j]);
                }
            }
        }
    }

    static void printPairs(int arr[], int sum)
    {
        HashSet<Integer> s = new HashSet<Integer>();
        for (int i = 0; i < arr.length; ++i) {
            int temp = sum - arr[i];

            // checking for condition
            if (s.contains(temp)) {
                System.out.println("Pair with given sum " + sum + " is (" + arr[i] + ", " + temp + ")");
            }
            s.add(arr[i]);
        }
    }


}
