package com.example.programing.perfectsubstringtest;
import java.util.HashMap;
import java.util.Scanner;

public class PerfectSubstringTest {


    public static void main(String[] args) {

        HashMap<String, Integer> map = new HashMap<>();
        int perfectSubStringCount = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the String");
        String mainString = scanner.nextLine();
        System.out.println(mainString);

        System.out.println("Please choose your substring length");
        int subStringLength = scanner.nextInt();
        System.out.println("You choose substring length " + subStringLength);

        for (int i = 0; i < mainString.length(); i++) {
            for (int j = i + 1; j < mainString.length(); ) {
                String subStringValue = mainString.substring(i, j + 1);

                System.out.println(subStringValue);
                map.clear();
                for (int k = 0; k < subStringValue.length(); k++) {
                    map.put(subStringValue.substring(k, k + 1), 0);
                }
                boolean isValid = true;
                for (String key : map.keySet()) {
                    int count = getCount(subStringValue, key);
                    if (count != subStringLength) {
                        isValid = false;
                        break;
                    }
                }
                if (isValid)
                    perfectSubStringCount += 1;
                j = j + subStringLength;
            }
        }

        System.out.println("The Perfect Substring count is "+perfectSubStringCount);

    }

    public static int getCount(String str, String findStr) {
        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {

            lastIndex = str.indexOf(findStr, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }
}
