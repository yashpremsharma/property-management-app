package com.example.programing.kotlininterview


fun main(args: Array<String>) {

    println(matchBrackets("(())()((("))
    println(matchBrackets("(()("))

//    println(encode("yashbhardwaj"))

}

fun matchBrackets(brackets: String): String {

    var open = 0
    var count = 0

    for (i in 0 until brackets.length) {
        if (brackets[i] == '(') {
            open++
        } else if (brackets[i] == ')') {
            open--
        }
        if (open < 0) {
            count++
            open++
        }
    }
    return (count+open).toString()
}

fun encode(s: String): String {
    val sb = StringBuilder()
    val word = s.toCharArray()
    var current = word[0] // We initialize to compare vs. first letter

    // our helper variables
    var index = 0 // tracks how far along we are
    var count = 0 // how many of the same letter we've seen

    for (c in word) {
        if (c == current) {
            count++
            index++

            if (index == word.size)
                sb.append(Integer.toString(count)+current)
        } else {
            sb.append(Integer.toString(count)+current)
            count = 1
            current = c
            index++
            if (index == word.size)
                sb.append(Integer.toString(count)+current)
        }
    }
    return sb.toString()
}
