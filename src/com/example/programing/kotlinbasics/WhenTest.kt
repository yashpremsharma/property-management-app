package com.example.programing.kotlinbasics

import java.util.Scanner;
fun main() {
    var reader = Scanner(System.`in`)
    println("Enter the month number:")
    var monthOfYear = reader.nextInt()
    var month = when(monthOfYear){
        1->"January"
        2->"Febuary"
        3->"March"
        4->"April"
        5->"May"
        6->"June"
        7->"July"
        8->"August"
        9->"September"
        10->"October"
        11->"November"
        12->"December"
        else -> {
            "Not a month of year"
        }
    }
    println(month)
}
